 .. _installation:
Installation & Getting Started
===============================

Narupa consists of two components, the python-based libraries
for running simulations, narupa-server, and the Unity3D libraries
and applications for visualising and interacting with simulations
in VR.

######################
Installing the Server
######################

Conda Installation
####################

.. code:: bash

    conda install -c irl -c omnia -c conda-forge narupa-server

Installing from source
########################

To install from source, follow the instructions on the README
of the `code repository <https://gitlab.com/intangiblerealities/narupa-protocol>`_.


########################################
Installing the Narupa Unity3D libraries.
########################################

The Narupa libraries for building your own VR applications in Unity3D are available `here <https://gitlab.com/intangiblerealities/narupa-unity-plugin>`_.

######################
Installing Narupa iMD
######################

Instructions for downloading and running the Narupa iMD Unity3D application are available `here <https://gitlab.com/intangiblerealities/narupa-applications/narupa-imd>`_.